package com.pragma.retoback.application.rest;

import com.pragma.retoback.application.request.SavePersonRequest;
import com.pragma.retoback.application.request.UpdatePersonRequest;
import com.pragma.retoback.domain.model.Person;
import com.pragma.retoback.domain.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(
            value = "/find",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Person find(
            @RequestParam(value = "id") final Long id,
            @RequestParam(value = "idType") final int idType){
        return personService.find(id, idType);
    }

    @GetMapping(
            value = "/find-all",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Person> findAll(){
        return personService.findAll();
    }

    @GetMapping(
            value = "/find-by-age",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Person> findByAge(
            @RequestParam(value = "age") final int age){
        return personService.findByAge(age);
    }


    @PostMapping(
            value = "/save",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String save(
            @RequestBody final SavePersonRequest request){
        return String.valueOf(personService.save(request.getPerson(), request.getImage()));
    }

//    @PutMapping(
//            value = "/update",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.TEXT_PLAIN_VALUE)
//    public String update(
//            @RequestBody final UpdatePersonRequest request){
//        return String.valueOf(personService.update(request));
//    }

    @DeleteMapping(
            value = "/delete",
            produces = MediaType.TEXT_PLAIN_VALUE)
    public String delete(
            @RequestParam(value = "id") final Long id,
            @RequestParam(value = "idType") final int idType){
        return String.valueOf(personService.delete(id, idType));
    }
}
