package com.pragma.retoback.application.rest;

import com.pragma.retoback.domain.model.Image;
import com.pragma.retoback.domain.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/image")
public class ImageController {
//    @Autowired
//    ImageService imageService;
//
//    @GetMapping(
//            value = "/find",
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public Image find(
//            @RequestParam(value = "id") String id){
//        return imageService.find(id);
//    }
//
//    @GetMapping(
//            value = "/find-by-person-id",
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<Image> findByPersonId(
//            @RequestParam(value = "personId") int personId){
//        return imageService.findByPersonId(personId);
//    }
//
//    @GetMapping(
//            value = "/find-all",
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<Image> findAll(){
//        return imageService.findAll();
//    }
//
//    @PostMapping(
//            value = "/save",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public Image save(@RequestBody Image image){
//        return imageService.save(image);
//    }
//
//    @PutMapping(
//            value = "/update",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public boolean update(@RequestBody Image image){
//        return imageService.update(image);
//    }
//
//    @GetMapping(
//            value = "/delete",
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public boolean delete(
//            @RequestParam(value = "id") String id){
//        return imageService.delete(id);
//    }
}
