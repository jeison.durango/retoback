package com.pragma.retoback.application.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.pragma.retoback.domain.model.Image;
import com.pragma.retoback.domain.model.Person;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class SavePersonRequest {
    @NotNull
    private final Person person;

    @NotNull
    private final Image image;

    @JsonCreator
    public SavePersonRequest(@NotNull Person person, @NotNull String imageCode) {
        this.person = person;
        this.image = new Image(person.getId(), person.getIdType(), imageCode);
    }
}
