package com.pragma.retoback.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "person")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "id_type")
    private int idType;

    @Column(name = "name")
    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "age")
    private int age;

    @Column(name = "birthplace")
    private String birthplace;

    @Column(name = "image_id")
    private String imageId;

    public Person(Long id, int idType, String name, String lastName, int age, String birthplace) {
        this.id = id;
        this.idType = idType;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.birthplace = birthplace;
    }
}
