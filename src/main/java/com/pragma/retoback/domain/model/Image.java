package com.pragma.retoback.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Document(collation = "image")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class Image {
    @Id
    @GeneratedValue
    private String id;

    private Long personId;
    private int personIdType;

    @NonNull
    private String code;

    public Image(Long personId, int personIdType, @NonNull String code) {
        this.personId = personId;
        this.personIdType = personIdType;
        this.code = code;
    }
}
