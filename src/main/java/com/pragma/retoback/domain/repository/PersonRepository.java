package com.pragma.retoback.domain.repository;

import com.pragma.retoback.domain.model.Person;
import java.util.List;
import java.util.Optional;

public interface PersonRepository{
    Optional<Person> find(Long id, int idType);
    Optional<List<Person>> findByAge(int age);
    Optional<List<Person>> findAll();
    void delete(Long id, int idType);
    void save(Person person);
    void update(Person person);

}
