package com.pragma.retoback.domain.repository;

import com.pragma.retoback.domain.model.Image;
import com.pragma.retoback.domain.model.Person;

import java.util.Optional;

public interface ImageRepository {
    Optional<Image> find(String id);
    Optional<Image> findByPerson(Long personId, int personIdType);
    void delete(String id);
    void save(Image image);
    void update(Image image);
}
