package com.pragma.retoback.domain.service;

import com.pragma.retoback.domain.model.Image;
import com.pragma.retoback.domain.repository.ImageRepository;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImpl implements ImageService{
    private final ImageRepository imageRepository;

    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Image find(String id) {
        return imageRepository.find(id).orElse(null);
    }

    @Override
    public Image findByPerson(Long personId, int personIdType) {
        return imageRepository.findByPerson(personId, personIdType).orElse(null);
    }

    //TODO implementar excepción
    @Override
    public boolean delete(String id) {
        if(find(id) == null){
            return false;
        }

        imageRepository.delete(id);
        return true;
    }

    //TODO implementar excepción
    @Override
    public boolean save(Image image) {
        if(findByPerson(image.getPersonId(), image.getPersonIdType()) != null){
            return false;
        }

        imageRepository.save(image);
        return true;
    }

    //TODO implementar excepción
    @Override
    public boolean update(Image image) {
        if(find(image.getId()) == null){
            return false;
        }

        imageRepository.update(image);
        return true;
    }
}
