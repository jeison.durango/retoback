package com.pragma.retoback.domain.service;

import com.pragma.retoback.domain.model.Image;

public interface ImageService {
    Image find(String id);
    Image findByPerson(Long personId, int personIdType);
    boolean delete(String id);
    boolean save(Image image);
    boolean update(Image image);
}
