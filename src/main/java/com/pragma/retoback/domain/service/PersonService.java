package com.pragma.retoback.domain.service;

import com.pragma.retoback.domain.model.Image;
import com.pragma.retoback.domain.model.Person;

import java.util.List;

public interface PersonService {
    List<Person> findAll();
    List<Person> findByAge(int age);
    Person find(Long id, int idType);
    boolean delete(Long id, int idType);
    boolean save(Person person, Image image);
    boolean update(Person person);
}
