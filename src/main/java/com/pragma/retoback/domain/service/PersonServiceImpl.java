package com.pragma.retoback.domain.service;

import com.pragma.retoback.domain.model.Image;
import com.pragma.retoback.domain.model.Person;
import com.pragma.retoback.domain.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService{
    private final PersonRepository personRepository;
    private final ImageService imageService;

    public PersonServiceImpl(final PersonRepository personRepository, final ImageService imageService) {
        this.personRepository = personRepository;
        this.imageService = imageService;
    }

    //TODO implement exception
    @Override
    public boolean save(Person person, Image image) {
        if(find(person.getId(), person.getIdType()) != null){
            return false;
        }

        imageService.save(image);
        person.setImageId(image.getId());
        personRepository.save(person);

        return true;
    }

    @Override
    public List<Person> findAll() {
        return personRepository.findAll().orElseThrow();
    }

    //TODO implement exception
    @Override
    public boolean update(Person person) {
        if(find(person.getId(), person.getIdType()) == null){
            return false;
        }

        personRepository.update(person);
        return true;
    }

    //TODO implement exception
    @Override
    public boolean delete(Long id, int idType) {
        if(find(id, idType) == null){
            return false;
        }

        personRepository.delete(id, idType);
        return true;
    }

    @Override
    public Person find(Long id, int idType) {
        return personRepository.find(id, idType).orElse(null);
    }

    //TODO implement exception
    @Override
    public List<Person> findByAge(int age) {
        return personRepository.findByAge(age).orElseThrow();
    }
}
