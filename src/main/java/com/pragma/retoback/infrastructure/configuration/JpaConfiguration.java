package com.pragma.retoback.infrastructure.configuration;

import com.pragma.retoback.infrastructure.repository.jpa.JpaPersonRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackageClasses = {JpaPersonRepository.class})
public class JpaConfiguration {
}
