package com.pragma.retoback.infrastructure.configuration;

import com.pragma.retoback.infrastructure.repository.mongo.MongoDbImageRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = {MongoDbImageRepository.class})
public class MongoDbConfiguration {
}
