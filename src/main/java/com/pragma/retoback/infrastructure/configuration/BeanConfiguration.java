package com.pragma.retoback.infrastructure.configuration;

import com.pragma.retoback.RetobackApplication;
import com.pragma.retoback.domain.repository.ImageRepository;
import com.pragma.retoback.domain.repository.PersonRepository;
import com.pragma.retoback.domain.service.ImageService;
import com.pragma.retoback.domain.service.ImageServiceImpl;
import com.pragma.retoback.domain.service.PersonService;
import com.pragma.retoback.domain.service.PersonServiceImpl;
import com.pragma.retoback.infrastructure.repository.jpa.JpaPersonRepository;
import com.pragma.retoback.infrastructure.repository.jpa.JpaPersonRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = {RetobackApplication.class})
public class BeanConfiguration {
    @Bean
    PersonService personService(final PersonRepository personRepository,
                                final ImageService imageService){
        return new PersonServiceImpl(personRepository, imageService);
    }

    @Bean
    ImageService imageService(final ImageRepository imageRepository){
        return new ImageServiceImpl(imageRepository);
    }
}
