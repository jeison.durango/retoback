package com.pragma.retoback.infrastructure.repository.jpa;

import com.pragma.retoback.domain.model.Person;
import com.pragma.retoback.domain.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Primary
public class JpaPersonRepositoryImpl implements PersonRepository {

    private final JpaPersonRepository jpaPersonRepository;

    @Autowired
    public JpaPersonRepositoryImpl(final JpaPersonRepository jpaPersonRepository) {
        this.jpaPersonRepository = jpaPersonRepository;
    }

    @Override
    public Optional<Person> find(Long id, int idType) {
        return jpaPersonRepository.findByIdAndIdType(id, idType);
    }

    @Override
    public Optional<List<Person>> findByAge(int age) {
        return jpaPersonRepository.findByAgeGreaterThanEqual(age);
    }

    @Override
    public Optional<List<Person>> findAll() {
        return Optional.of(jpaPersonRepository.findAll());
    }

    @Override
    public void delete(Long id, int idType) {
        jpaPersonRepository.deleteByIdAndIdType(id, idType);
    }

    @Override
    public void save(Person person) {
        jpaPersonRepository.save(person);
    }

    @Override
    public void update(Person person) {
        jpaPersonRepository.save(person);
    }
}
