package com.pragma.retoback.infrastructure.repository.mongo;

import com.pragma.retoback.domain.model.Image;
import com.pragma.retoback.domain.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Primary
public class MongoDbImageRepositoryImpl implements ImageRepository {
    private final MongoDbImageRepository imageRepository;

    @Autowired
    public MongoDbImageRepositoryImpl(final MongoDbImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Optional<Image> find(String id) {
        return imageRepository.findById(id);
    }

    @Override
    public Optional<Image> findByPerson(Long personId, int personIdType) {
        return imageRepository.findByPersonIdAndPersonIdType(personId, personIdType);
    }

    @Override
    public void delete(String id) {
        imageRepository.deleteById(id);
    }

    @Override
    public void save(Image image) {
        imageRepository.save(image);
    }

    @Override
    public void update(Image image) {
        imageRepository.save(image);
    }
}
