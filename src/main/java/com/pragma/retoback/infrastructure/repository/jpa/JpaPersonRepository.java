package com.pragma.retoback.infrastructure.repository.jpa;

import com.pragma.retoback.domain.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface JpaPersonRepository extends JpaRepository<Person, Long> {
    @Transactional
    Optional<List<Person>> findByAgeGreaterThanEqual(int age);

    @Transactional
    Optional<Person> findByIdAndIdType(Long id, int idType);

    @Transactional
    @Modifying
    void deleteByIdAndIdType(Long id, int idType);
}
