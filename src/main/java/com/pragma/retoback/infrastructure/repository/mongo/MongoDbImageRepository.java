package com.pragma.retoback.infrastructure.repository.mongo;

import com.pragma.retoback.domain.model.Image;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface MongoDbImageRepository extends MongoRepository<Image, String> {
    @Transactional
    Optional<Image> findByPersonIdAndPersonIdType(Long id, int idType);
}
